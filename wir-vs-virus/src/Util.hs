module Util where

-- text
import Data.Text

-- lucid
import Lucid

showHtml :: Show a => a -> Html ()
showHtml = toHtml . pack . show
