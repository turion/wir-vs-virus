{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Crop where

-- text
import Data.Text

-- lucid
import Lucid

--wir-vs-virus
import Util

data Crop = Crop
  { name :: Html ()
  , size :: Size
  , worth :: Integer
  , picture :: Text
  }

data Size
  = Gram500
  | Piece
  | Bushel

sizeHtml :: Size -> Html ()
sizeHtml Gram500 = "500 g"
sizeHtml Piece = "1 Stück"
sizeHtml Bushel = "1 Bund"

potato :: Crop
potato = Crop
  { name = "Kartoffel"
  , size = Gram500
  , worth = 1
  , picture = "https://images.unsplash.com/photo-1508313880080-c4bef0730395?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1867&q=80"
  }

parsley :: Crop
parsley = Crop
  { name = "Petersilie"
  , size = Bushel
  , worth = 2
  , picture = "https://images.unsplash.com/photo-1535189487909-a262ad10c165?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
  }

cropHtml :: Crop -> Html ()
cropHtml Crop {..} = div_ [class_ "card"] $ do
  div_ [class_ "card-image"] $ do
    figure_ [class_ "image is-4by3"] $ img_ [src_ picture]
  div_ [class_ "card-content"] $ do
    nav_ [class_ "level"] $ do
      div_ [class_ "level-item"] $ do
        p_ [class_ "title is-4"] $ name
      div_ [class_ "level-item"] $ do
        p_ [class_ "is-4"] $ "Credits: " <> showHtml worth

allCropsHtml :: Html ()
allCropsHtml = mapM_ cropHtml [potato, parsley]
