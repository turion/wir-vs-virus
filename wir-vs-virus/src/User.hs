{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module User where

-- base
import Data.Maybe

-- text
import Data.Text

-- lucid
import Lucid

data Role
  = Farmer
  | Worker
  deriving (Show)

type Nick = Text

data User = User
  { name :: Text
  , nick :: Nick
  , role :: Role
  , avatar :: Maybe Text
  }

linda :: User
linda = User
  { name = "Linda Farmer"
  , nick = "linda"
  , role = Farmer
  , avatar = Nothing
  }

pete :: User
pete = User
  { name = "Pete Worker"
  , nick = "pete"
  , role = Worker
  , avatar = Nothing
  }

userHtml :: User -> Html ()
userHtml User {..} = div_ [class_ "card"] $ do
  div_ [class_ "card-image"] $ do
    figure_ [class_ "image is-5by3"] $ do img_ [src_ $ fromMaybe defaultAvatar avatar]
  div_ [class_ "card-content"] $ do
    div_ [class_ "media"] $ do
      div_ [class_ "media-left"] $ do
        span_ [class_ "icon-large"] $ do i_ [class_ $ roleIcon role] ""
      div_ [class_ "media-content"] $ do
        p_ [class_ "title is-4"] $ toHtml name

defaultAvatar :: Text
defaultAvatar = "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__480.png"

roleIcon :: Role -> Text
roleIcon Farmer = "fas fa-tractor"
roleIcon Worker = "fas fa-hands-helping"

allUsersHtml :: Html ()
allUsersHtml = mapM_ userHtml
  [ User
    { name = "Anna Muster"
    , nick = "anna"
    , role = Worker
    , avatar = Just "https://images.unsplash.com/photo-1558819247-165f6fd9b0ff?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
    }
  , User
    { name = "Adrian Lindenhof"
    , nick = "adrian"
    , role = Farmer
    , avatar = Just "https://images.unsplash.com/photo-1537721664796-76f77222a5d0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
    }
  ]
