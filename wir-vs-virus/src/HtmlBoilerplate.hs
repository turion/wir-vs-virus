{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE OverloadedStrings #-}
module HtmlBoilerplate where

-- base
import Data.List (intersperse)

-- text
import Data.Text (Text)

-- lucid
import Lucid

-- wir-vs-virus
import Content (content)

-- * HTML boilerplate

footer :: Html ()
footer = do
  with footer_ [class_ "footer"] $ div_ $ p_ $ do
    "Proudly made with "
    mconcat $ intersperse ", " $ (\(address, title) -> a_ [ href_ address ] title) <$>
      [ ("https://nixos.org/", "NixOS")
      , ("https://www.servant.dev/", "Servant")
      , ("https://github.com/chrisdone/lucid", "Lucid")
      , ("http://fvisser.nl/clay/", "Clay")
      , ("https://bulma.io/", "Bulma")
      , ("https://fontawesome.com/", "Font Awesome")
      ]
    " and "
    a_ [ href_ "https://www.haskell.org/" ] "Haskell"

page :: Html ()
page = doctypehtml_ $ do
  head'
  body

body :: Html ()
body = body_ $ do
  content
  footer

head' :: Html ()
head' = head_ $ do
  meta_ [name_ "viewport", content_ "width=device-width, initial-scale=1"]
  link_
    [ rel_ "stylesheet"
    , href_ "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css"
    ]
  script_
    [ src_ "https://use.fontawesome.com/releases/v5.12.1/js/all.js"
    ] ("" :: Text)
  title_ "Wir vs. Virus"
