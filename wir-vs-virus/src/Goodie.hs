{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Goodie where

-- text
import Data.Text

-- lucid
import Lucid

-- wir-vs-virus
import Crop
import Util

data Goodie
  = Box [Crop]
  | Pension
  | Salary
  | Training
  | Massage
  | Device
  | Givenbox

type Credit = Integer

goodieCredit :: Goodie -> Credit
goodieCredit (Box crops) = sum $ worth <$> crops
goodieCredit Pension = 2
goodieCredit Salary = 3
goodieCredit Training = 4
goodieCredit Massage = 5
goodieCredit Device = 6
goodieCredit Givenbox = 1

goodiePicture :: Goodie -> Text
goodiePicture Pension = "https://www.rentenversicherungsbeitrag.com/wp-content/uploads/beitrag-rentenversicherung.jpg"
goodiePicture Salary = "https://img.topbank.vn/2017/08/08/6ROVILlO/gehalt-gehaltserhoeh-5512.jpg"
goodiePicture Training = "https://www.rw-textilservice.de/files/smthumbnaildata/940x623/2/4/0/0/2/6/1/AdobeStock_206371462-fotogestoeber--stock.adobe.com_RWT-Idee-Teamwork-Weiterbildung.jpg"
goodiePicture Massage = "https://www.float-hamburg.com/wp-content/uploads/2018/05/Klassische-Massage-1-400-x-300.jpg"
goodiePicture Device = "https://www.onlinehaendler-news.de/images/Artikelbilder/shutterstock_156997538.jpg"
goodiePicture Givenbox = "https://www.hofladen-bayern.de/media/image/91/5a/ed/02004-hofladen-bayern-gemuesekiste.jpg"

goodieTitle :: Goodie -> Html ()
goodieTitle (Box crops) = "Gemüseebox selber zusammenstellen"
goodieTitle Pension = "Einzahlung bei Rentenversicherung zählt doppelt"
goodieTitle Salary = "Wöchentlich eine kleine Gehaltserhöhungen"
goodieTitle Training = "Bezahlte Fortbildung"
goodieTitle Massage = "Massagen bei Physiotherapeuten"
goodieTitle Device = "Gerät aussuchen (Handy, Laptop, Tablet)"
goodieTitle Givenbox = "Gemüsekiste"

goodieHtml :: Goodie -> Html ()
goodieHtml goodie = div_ [class_ "box"] $ do
  p_ [class_ "title is-4"] $ (goodieTitle goodie)
  p_ [class_ "is-4"] $ "Benötigte Credits: " <> showHtml (goodieCredit goodie)
  goodieHtmlInner goodie

goodieHtmlInner :: Goodie -> Html ()
goodieHtmlInner (Box crops) = div_ [class_ "tile is-ancestor"] $ cropsBoxHtml crops
goodieHtmlInner goodie = img_ [src_ $ (goodiePicture goodie)]

parentChild :: Html () -> Html ()
parentChild inner
  = div_ [class_ "tile is-parent"]
  $ div_ [class_ "tile is-child box"]
    inner

cropsBoxHtml :: [Crop] -> Html ()
cropsBoxHtml [] = parentChild $ return ()

cropsBoxHtml [crop] = parentChild $ cropHtml crop

cropsBoxHtml [crop1, crop2] = do
  parentChild $ cropHtml crop1
  parentChild $ cropHtml crop2

cropsBoxHtml [crop1, crop2, crop3] = fourTile
  (cropHtml crop1)
  (cropHtml crop2)
  (cropHtml crop3)
  (return ())

cropsBoxHtml [crop1, crop2, crop3, crop4] = fourTile
  (cropHtml crop1)
  (cropHtml crop2)
  (cropHtml crop3)
  (cropHtml crop4)

cropsBoxHtml (crop1 : crop2 : crop3 : _) = fourTile
  (cropHtml crop1)
  (cropHtml crop2)
  (cropHtml crop3)
  "..."

fourTile :: Html () -> Html () -> Html () -> Html () -> Html ()
fourTile html1 html2 html3 html4 = do
  div_ [class_ "tile is-parent is-vertical"] $ do
    div_ [class_ "tile is-child box"] html1
    div_ [class_ "tile is-child box"] html2
  div_ [class_ "tile is-parent is-vertical"] $ do
    div_ [class_ "tile is-child box"] html3
    div_ [class_ "tile is-child box"] html4


allGoodiesHtml :: Html ()
allGoodiesHtml = mapM_ goodieHtml
  [Givenbox, Pension, Salary, Training, Massage,
  Device
  ]
