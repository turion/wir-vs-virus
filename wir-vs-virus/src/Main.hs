{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}

module Main where

-- warp
import Network.Wai.Handler.Warp

-- wir-vs-virus
import App

-- * We might need some of these imports later
{-
-- base
import Control.Monad
import Data.Char (isUpper)
import GHC.Generics
import Prelude hiding (rem)

-- text
import qualified Data.Text as T
import Data.Text.Lazy (toStrict)

-- clay
import Clay hiding (title, body, contents, footer, url, content, not)

-- servant-lucid
import Lucid.Servant

-- http-api-data
import Web.HttpApiData
-}

main :: IO ()
main = do
  putStrLn "There we go"
  run 8081 app
