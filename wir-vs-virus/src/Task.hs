{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Task where

-- time
import Data.Time.Calendar

-- text
import Data.Text

-- lucid
import Lucid

-- wir-vs-virus
import User
import Util

data Status
  = NotStarted
  | Assigned
  | Finished
  | Verified

type TaskId = Integer

data Task = Task
  { target :: Html ()
  , nickFarmer :: Nick
  , nickWorker :: Maybe Nick
  , credits :: Integer
  , status :: Status
  , start :: Day
  , due :: Day
  , taskId :: Maybe TaskId
  }

statusHtml :: Status -> Html ()
statusHtml NotStarted = "Noch nicht begonnen"
statusHtml Assigned = "Zugewiesen"
statusHtml Finished = "Abgeschlossen"
statusHtml Verified = "Abgeschlossen (verifiziert)"

dayHtml :: Day -> Html ()
dayHtml = toHtml . pack . showGregorian

taskHtml :: Task -> Html ()
taskHtml Task {..} = div_ [class_ "box"] $ do
  p_ [class_ "is-4"] target
  p_ [class_ "is-4"] $ "Credits: " <> showHtml credits
  p_ [class_ "is-small"] $ statusHtml status
  p_ [class_ "is-small"] $ dayHtml start <> " - " <> dayHtml due

exampleTask :: Task
exampleTask = Task
  { target = "Erdbeerernte"
  , nickFarmer = "linda"
  , nickWorker = Just "pete"
  , credits = 30
  , status = Assigned
  , start = fromGregorian 2020 03 23
  , due   = fromGregorian 2020 04 03
  , taskId = Nothing
  }

allTasksHtml :: Html ()
allTasksHtml = taskHtml exampleTask
