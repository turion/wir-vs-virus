{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}
module Control.Monad.Accum.Class where

-- transformers
import qualified Control.Monad.Trans.Accum as Accum
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.State
import Control.Monad.Trans.Reader

class (Monoid w, Monad m) => MonadAccum w m | m -> w where
  look :: m w
  add :: w -> m ()

looks :: MonadAccum w m => (w -> a) -> m a
looks f = fmap f look

instance (Monoid w, Monad m) => MonadAccum w (Accum.AccumT w m) where
  look = Accum.look
  add = Accum.add

instance MonadAccum w m => MonadAccum w (ReaderT r m) where
  look = lift look
  add = lift . add

instance MonadAccum w m => MonadAccum w (ExceptT e m) where
  look = lift look
  add = lift . add

instance MonadAccum w m => MonadAccum w (StateT s m) where
  look = lift look
  add = lift . add
