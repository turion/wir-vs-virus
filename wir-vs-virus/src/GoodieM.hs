module GoodieM where

-- base
import Control.Monad (void)
import Data.Function

-- transformers
import Control.Monad.Trans.Accum (Accum)
import Control.Monad.Trans.Except
import Control.Monad.Trans.Reader
import Control.Monad.Trans.State (StateT)

-- mtl
import Control.Monad.Error.Class
import Control.Monad.State.Class

-- wir-vs-virus
import Control.Monad.Accum.Class -- TODO Upload to mtl as PR
import Task
import User
import Goodie

data GoodieException
  = NickNotFound Nick
  | TaskNotFound TaskId
  | TaskHasNoId
  | TaskHasId

type UserStore = [User]

data TaskStore = TaskStore
  { tasks :: [Task]
  , taskMaxId :: TaskId
  }

initTaskStore :: TaskStore
initTaskStore = TaskStore
  { tasks = []
  , taskMaxId = 0
  }

data GoodieOrder = GoodieOrder
  { goodie :: Goodie
  , nickUser :: Nick
  }

type GoodieOrderStore = [GoodieOrder]

type GoodieM a = ReaderT UserStore (ExceptT GoodieException (StateT TaskStore (Accum GoodieOrderStore))) a

lookupBy :: Eq b => b -> (a -> b) -> [a] -> Maybe a
lookupBy b keyFunction as
  = zip (keyFunction <$> as) as
  & lookup b

getUser :: Nick -> GoodieM User
getUser nick_ = do
  users <- ask
  let maybeUser = lookupBy nick_ User.nick users
  maybe (throwError $ NickNotFound nick_) return maybeUser

assertUserExists :: Nick -> GoodieM ()
assertUserExists = void . getUser

addTask :: Task -> GoodieM Task
addTask Task { taskId = Just _ } = throwError TaskHasId
addTask task@Task { taskId = Nothing } = do
  maxId <- gets taskMaxId
  let taskWithId = task { taskId = Just maxId }
  store <- get
  put $ TaskStore
    { tasks = taskWithId : tasks store
    , taskMaxId = maxId + 1
    }
  return taskWithId

updateTask :: Task -> GoodieM ()
updateTask Task { taskId = Nothing } = throwError TaskHasNoId
updateTask task@Task { taskId = Just taskId_ } = do
  store <- get
  case break ((Just taskId_ ==) . Task.taskId) (tasks store) of
    (_, []) -> throwError $ TaskNotFound taskId_
    (tasksBefore, _task : tasksAfter) -> put store { tasks = tasksBefore ++ [task] ++ tasksAfter }

getTasks :: GoodieM [Task]
getTasks = gets tasks

assertOr :: Bool -> GoodieException -> GoodieM ()
assertOr b e = if b then return () else throwError e

assignTask :: Task -> Nick -> GoodieM ()
assignTask task nick_ = do
  assertUserExists nick_
  updateTask $ task { nickWorker = Just nick_ }

addGoodieOrder :: GoodieOrder -> GoodieM ()
addGoodieOrder goodieOrder = add [goodieOrder]
