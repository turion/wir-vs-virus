{-# LANGUAGE DataKinds #-}
module App where

-- lucid
import Lucid

-- servant-server
import Servant

-- servant-lucid
import Servant.HTML.Lucid

-- wir-vs-virus
import HtmlBoilerplate

-- * servant-specific stuff

type API
  =    Get '[HTML] (Html ())

api :: Proxy API
api = Proxy

server :: Server API
server
  =    return page

app :: Application
app = serve api server
