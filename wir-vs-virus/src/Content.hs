{-# LANGUAGE OverloadedStrings #-}
module Content where

-- lucid
import Lucid

-- wir-vs-virus
import Crop
import Goodie
import Task
import User

-- * Website content

content :: Html ()
content = div_ [class_ "container content"] $ do
  h1_ [class_ "title"] $ "Wir vs. Virus"
  div_ [class_ "columns"] $ do
    div_ [class_ "column"] allUsersHtml
    div_ [class_ "column"] allCropsHtml
    div_ [class_ "column"] allGoodiesHtml
    div_ [class_ "column"] allTasksHtml
