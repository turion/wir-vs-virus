{ config, pkgs, ... }:

let
  wir-vs-virus = import ./default.nix {};

in
{
  systemd.services."wir-vs-virus-servant" = {
    description = "Wir vs. virus servant";
    serviceConfig = {
      Type = "simple";
      ExecStart = "${wir-vs-virus}/bin/wir-vs-virus";
    };
    wantedBy = [ "default.target" ];
  };
}
