# To use this, simply add ./path/to/container-host-config.nix to your configuration.nix.
# After you've done `nixos-rebuild switch`, you should be able to access localhost:8080.
# You can then log in to the container with `sudo nixos-container root-login wir-vs-virus`.
{ config, pkgs, ... }:

{
  containers.wir-vs-virus = {
    config = import ./container-config.nix;
    autoStart = true;
  };
}
