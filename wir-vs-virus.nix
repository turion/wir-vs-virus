{ pkgs, compiler, test }:

let
  overridePkgs = pkgs.haskell.packages.${compiler}.override {
    overrides = super: self: {
      wir-vs-virus = self.callCabal2nix "wir-vs-virus" ./wir-vs-virus {};
    };
  };
  drv = overridePkgs.wir-vs-virus;
  maybeTestedDrv = if test then drv else pkgs.haskell.lib.dontCheck drv;
in

if pkgs.lib.inNixShell then maybeTestedDrv.env else maybeTestedDrv
