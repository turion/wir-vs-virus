let
  wir-vs-virus = import ./wir-vs-virus.nix;
  nixpkgs = import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs-channels/archive/d96bd3394b734487d1c3bfbac0e8f17465e03afe.tar.gz";
    sha256 = "05n27wz5ln9ni5cy5rhjcy612i44gmblkq5m0g827v8pd0nk00da";
  }) {};
in
{ pkgs ? nixpkgs.pkgs, compiler ? "ghc881", test ? true }:
wir-vs-virus {
  inherit pkgs compiler test;
}
